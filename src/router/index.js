
import { createRouter, createWebHistory } from 'vue-router'
import { defineAsyncComponent } from 'vue'
 
const router = createRouter({
  history: createWebHistory(),
  routes: [
      {
      path: '/',
      redirect: 'home'
    },
    {
      path: '/home',
      name: 'home',
      component: defineAsyncComponent(() => import(/* @vite-ignore */ `../views/Home.vue`))
    },
       {
      path: '/my',
      name: 'my',
      component: defineAsyncComponent(() => import(/* @vite-ignore */ `../views/my/my.vue`))
    },
       {
      path: '/login',
      name: 'login',
      component: defineAsyncComponent(() => import(/* @vite-ignore */ `../views/login/login.vue`))
    },
  ]
})
// 全局路由守卫
router.beforeEach((to, from, next)=>{
  // console.log(to, from)
  if (to.meta.title) {
    document.title = `${to.meta.title}`;
  }
  next()
})
 export default router